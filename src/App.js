import React, {Fragment} from 'react';

import {Provider} from 'react-redux';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import {PersistGate} from 'redux-persist/integration/react';
import Loader from './components/loader';

import store, {persistor} from './store';

import HomeContainer from './containers/home'
import EventEntryContainer from './containers/evententry'
import TestPageContainer from './containers/testpage'
import LoginContainer from './containers/login';
import RegisterContainer from './containers/register';

import PrivateRoute from './components/common/privateroute';

import 'semantic-ui-css/semantic.min.css';
import './App.css';

export default () => (
  <Provider store={store}>
    <PersistGate loading={<Loader />} persistor={persistor}>
      <BrowserRouter>
        <Fragment>
          <Switch>
            <PrivateRoute path="/home" exact component={HomeContainer} />
            <PrivateRoute path="/evententry" exact component={EventEntryContainer} />
            <PrivateRoute path="/testpage" exact component={TestPageContainer} />
            <Route path="/login" component={LoginContainer} />
            <Route path="/register" component={RegisterContainer} />
            <Route
              path="/"
              exact
              render={() => <Redirect to="/home" />}
            />
          </Switch>
        </Fragment>
      </BrowserRouter>
    </PersistGate>
  </Provider>
);
