import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from './types';

import {loginApi} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';

export const login = (username, password) => dispatch => {
  dispatch(loginRequest());

  loginApi(username, password)
    .then(response => {
      dispatch(loginSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(loginFailure(errorMessage));
    });
};

export const loginRequest = () => {
  return {
    type: LOGIN_REQUEST,
  };
};

export const loginSuccess = data => {
  return {
    type: LOGIN_SUCCESS,
    token: data.token,
    username: data.username,
    name: data.name,
    avatar: data.avatar,
    isStaff: data.is_staff,
  };
};

export const loginFailure = error => {
  return {
    type: LOGIN_FAILURE,
    error,
  };
};
