import {
  CREATE_CREDENTIAL_REQUEST,
  CREATE_CREDENTIAL_SUCCESS,
  CREATE_CREDENTIAL_FAILURE,
  FETCH_CREDENTIAL_SUCCESS,
  FETCH_CREDENTIAL_FAILURE,
} from './types';

import {createCredentialApi, fetchCredentialApi} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';

export const createCredential = newCredential => dispatch => {
  dispatch(createCredentialRequest());

  createCredentialApi(newCredential)
    .then(response => {
      dispatch(createCredentialSuccess(response.data));

      // re-load credential info
      fetchCredentialApi()
        .then(response => {
          dispatch(fetchCredentialSuccess(response.data));
        })
        .catch(error => {
          const errorMessage = apiErrorHandler(error);
          dispatch(fetchCredentialFailure(errorMessage));
        });
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(createCredentialFailure(errorMessage));
    });
};

export const createCredentialRequest = () => {
  return {
    type: CREATE_CREDENTIAL_REQUEST,
  };
};

export const createCredentialSuccess = () => {
  return {
    type: CREATE_CREDENTIAL_SUCCESS,
  };
};

export const createCredentialFailure = error => {
  return {
    type: CREATE_CREDENTIAL_FAILURE,
    error,
  };
};


export const fetchCredentialSuccess = (credential) => {
  return {
    type: FETCH_CREDENTIAL_SUCCESS,
    credential,
  };
};

export const fetchCredentialFailure = error => {
  return {
    type: FETCH_CREDENTIAL_FAILURE,
    error,
  };
};
