import {
  FETCH_ENTRY_REQUEST,
  FETCH_ENTRY_SUCCESS,
  FETCH_ENTRY_FAILURE,
  CREATE_OR_UPDATE_ENTRY_REQUEST,
  CREATE_OR_UPDATE_ENTRY_SUCCESS,
  CREATE_OR_UPDATE_ENTRY_FAILURE,
  DELETE_ENTRY_REQUEST,
  DELETE_ENTRY_SUCCESS,
  DELETE_ENTRY_FAILURE,
} from './types';

import {fetchEntryApi, createOrUpdateEntryApi, deleteEntryApi} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';


export const deleteEntry = (id) => dispatch => {
  dispatch(deleteEntryRequest(id));

  deleteEntryApi(id)
    .then(response => {
      dispatch(deleteEntrySuccess(id));

      // re-load entry page
      fetchEntryApi()
        .then(response => {
          dispatch(fetchEntrySuccess(response.data.data));
        })
        .catch(error => {
          const errorMessage = apiErrorHandler(error);
          dispatch(fetchEntryFailure(errorMessage));
        });
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(deleteEntryFailure(id, errorMessage));
    });
};

export const deleteEntryRequest = id => {
  return {
    type: DELETE_ENTRY_REQUEST,
    id,
  };
};

export const deleteEntrySuccess = id => {
  return {
    type: DELETE_ENTRY_SUCCESS,
    id,
  };
};

export const deleteEntryFailure = (id, error) => {
  return {
    type: DELETE_ENTRY_FAILURE,
    id,
    error,
  };
};


export const createOrUpdateEntry = entryparm => dispatch => {
  dispatch(createOrUpdateEntryRequest());

  createOrUpdateEntryApi(entryparm)
    .then(response => {
      dispatch(createOrUpdateEntrySuccess());

      // re-load entry page
      fetchEntryApi()
        .then(response => {
          dispatch(fetchEntrySuccess(response.data.data));
        })
        .catch(error => {
          const errorMessage = apiErrorHandler(error);
          dispatch(fetchEntryFailure(errorMessage));
        });
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(createOrUpdateEntryFailure(errorMessage));
    });
};

export const createOrUpdateEntryRequest = () => {
  return {
    type: CREATE_OR_UPDATE_ENTRY_REQUEST,
  };
};

export const createOrUpdateEntrySuccess = () => {
  return {
    type: CREATE_OR_UPDATE_ENTRY_SUCCESS,
  };
};

export const createOrUpdateEntryFailure = error => {
  return {
    type: CREATE_OR_UPDATE_ENTRY_FAILURE,
    error,
  };
};




export const fetchEntry = () => dispatch => {
  dispatch(fetchEntryRequest());

  fetchEntryApi()
    .then(response => {
      dispatch(fetchEntrySuccess(response.data.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchEntryFailure(errorMessage));
    });
};

export const fetchEntryRequest = () => {
  return {
    type: FETCH_ENTRY_REQUEST,
  };
};

export const fetchEntrySuccess = entry => {
  return {
    type: FETCH_ENTRY_SUCCESS,
    entry,
  };
};

export const fetchEntryFailure = error => {
  return {
    type: FETCH_ENTRY_FAILURE,
    error,
  };
};
