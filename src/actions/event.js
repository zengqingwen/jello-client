import {
  FETCH_EVENT_REQUEST,
  FETCH_EVENT_SUCCESS,
  FETCH_EVENT_FAILURE,
} from './types';

import {fetchEventApi} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';


export const fetchEvent = (currentFocusDate, currentFocusView) => dispatch => {
  dispatch(fetchEventRequest());

  fetchEventApi(currentFocusDate, currentFocusView)
    .then(response => {
      dispatch(fetchEventSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchEventFailure(errorMessage));
    });
};

export const fetchEventRequest = () => {
  return {
    type: FETCH_EVENT_REQUEST,
  };
};

export const fetchEventSuccess = event => {
  return {
    type: FETCH_EVENT_SUCCESS,
    event,
  };
};

export const fetchEventFailure = error => {
  return {
    type: FETCH_EVENT_FAILURE,
    error,
  };
};
