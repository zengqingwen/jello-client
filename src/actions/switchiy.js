import {
  SHOW_ADD_CREDENTIAL_MODAL,
} from './types';
//
// export const showAddCredentialModal = () => {
//   return {
//     type: SHOW_ADD_CREDENTIAL_MODAL,
//   }
// }

export const showAddCredentialModal = () => ({
  type: SHOW_ADD_CREDENTIAL_MODAL,
});

