// Action Types

// login
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

// register
export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';

// event
export const FETCH_EVENT_REQUEST = 'FETCH_EVENT_REQUEST';
export const FETCH_EVENT_SUCCESS = 'FETCH_EVENT_SUCCESS';
export const FETCH_EVENT_FAILURE = 'FETCH_EVENT_FAILURE';

// entry
export const FETCH_ENTRY_REQUEST = 'FETCH_ENTRY_REQUEST';
export const FETCH_ENTRY_SUCCESS = 'FETCH_ENTRY_SUCCESS';
export const FETCH_ENTRY_FAILURE = 'FETCH_ENTRY_FAILURE';

export const CREATE_OR_UPDATE_ENTRY_REQUEST = 'CREATE_OR_UPDATE_ENTRY_REQUEST';
export const CREATE_OR_UPDATE_ENTRY_SUCCESS = 'CREATE_OR_UPDATE_ENTRY_SUCCESS';
export const CREATE_OR_UPDATE_ENTRY_FAILURE = 'CREATE_OR_UPDATE_ENTRY_FAILURE';

export const DELETE_ENTRY_REQUEST = 'DELETE_ENTRY_REQUEST';
export const DELETE_ENTRY_SUCCESS = 'DELETE_ENTRY_SUCCESS';
export const DELETE_ENTRY_FAILURE = 'DELETE_ENTRY_FAILURE';

export const SHOW_ADD_CREDENTIAL_MODAL = 'SHOW_ADD_CREDENTIAL_MODAL';

export const CREATE_CREDENTIAL_REQUEST = 'CREATE_CREDENTIAL_REQUEST';
export const CREATE_CREDENTIAL_SUCCESS = 'CREATE_CREDENTIAL_SUCCESS';
export const CREATE_CREDENTIAL_FAILURE = 'CREATE_CREDENTIAL_FAILURE';

export const FETCH_CREDENTIAL_SUCCESS = 'FETCH_CREDENTIAL_SUCCESS';
export const FETCH_CREDENTIAL_FAILURE = 'FETCH_CREDENTIAL_FAILURE';
