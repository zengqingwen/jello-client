export const API_URL =
  process.env.NODE_ENV === 'production'
    ? 'http://yourdomain.com:8000/api/'
    : 'http://127.0.0.1:8000/api/';

export const USER_URL = API_URL + 'user/';
export const USER_LOGIN_URL = USER_URL + 'login/';
export const GET_CURRENT_USER_URL = API_URL + 'currentuser/';
export const USER_REGISTER_URL = USER_URL + 'register/';

export const EVENT_URL = API_URL + 'event/event/';
export const ENTRY_URL = API_URL + 'event/entry/';
export const CREDENTIAL_URL = API_URL + 'event/credential/';
