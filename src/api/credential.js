import axios from 'axios';

import {
  CREDENTIAL_URL,
} from './constants';

import {getConfig} from '../utils/config';


export const createCredentialApi = (newCredential) => {
  return axios.post(CREDENTIAL_URL, newCredential, getConfig());
}

export const fetchCredentialApi = () => {
  return axios.get(CREDENTIAL_URL, getConfig());
}
