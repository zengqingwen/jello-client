import axios from 'axios';

import {
  ENTRY_URL,
} from './constants';

import {getConfig} from '../utils/config';


export const fetchEntryApi = () => {
  return axios.get(ENTRY_URL, getConfig());
};

export const createOrUpdateEntryApi = (entryparm) => {
  return axios.post(ENTRY_URL, entryparm, getConfig());
}

export const deleteEntryApi = (id) => {
  return axios.delete(ENTRY_URL + id + '/', getConfig());
}
