import axios from 'axios';

import {
  EVENT_URL,
} from './constants';

import {getConfig} from '../utils/config';

export const fetchEventApi = (currentFocusDate, currentFocusView) => {
  const config = getConfig()
  config.params = {
    currentFocusDate,
    currentFocusView
  }
  return axios.get(EVENT_URL, config);
};
