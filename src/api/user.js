import axios from 'axios';

import {
  USER_LOGIN_URL,
  GET_CURRENT_USER_URL,
  USER_REGISTER_URL,
} from './constants';
import {getConfig} from '../utils/config';

export const loginApi = (username, password) => {
  return axios.post(USER_LOGIN_URL, {username, password}, getConfig());
};

export const getCurrentUserAPI = () => {
  return axios.get(GET_CURRENT_USER_URL, getConfig());
}

export const registerApi = data => {
  return axios.post(USER_REGISTER_URL, data, getConfig());
};
