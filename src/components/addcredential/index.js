import React, {Component} from 'react';
import {Button, Grid, Form, Modal} from 'semantic-ui-react';
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';

import {showAddCredentialModal, createCredential} from '../../actions';

class CredentialAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      creName: "",
      creToken: "",
    };
  }

  showAddCredentialModal = () => {
    const {showAddCredentialModal} = this.props;
    showAddCredentialModal();
  }

  componentDidUpdate() {
    const {addCredentialModal} = this.props;
    console.log(addCredentialModal)
  }

  handleChange = (e, {name, value}) => {
    this.setState({[name]: value})
  }

  onSubmit = () => {
    const {creName, creToken} = this.state;
    const {createCredential} = this.props;
    let newCredential = {
      name: creName,
      token: creToken,
    };
    console.log(newCredential)
    createCredential(newCredential);
  }

  render() {
    const {creName, creToken} = this.state;
    const {addCredentialModal} = this.props;

    return (
      <Modal dimmer={true} open={addCredentialModal} onClose={this.showAddCredentialModal}>
        <Modal.Header>Add a credential.</Modal.Header>
        <Modal.Content image>
          <Grid.Column>
            <Form.Input
              label="credential name"
              placeholder="Input your credential name"
              type="text"
              name="creName"
              value={creName}
              onChange={this.handleChange}
            />
            <Form.Input
              label="credential token"
              placeholder="Input your credential token"
              type="text"
              name="creToken"
              value={creToken}
              onChange={this.handleChange}
            />
          </Grid.Column>
        </Modal.Content>
        <Modal.Actions>
          <Button color='black' onClick={this.showAddCredentialModal}>
            取消
          </Button>
          <Button
            positive
            icon='checkmark'
            labelPosition='right'
            content="添加"
            onClick={this.onSubmit}
          />
        </Modal.Actions>
      </Modal>
    )
  }
}

const mapStateToProps = state => {
  return {
    addCredentialModal: state.switchiy.addCredentialModal,
  };
};

const mapDispatchToProps = dispatch => ({
  showAddCredentialModal: bindActionCreators(showAddCredentialModal, dispatch),
  createCredential: (newCredential) => {
    dispatch(createCredential(newCredential));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CredentialAdd);
