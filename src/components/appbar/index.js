import React from 'react';
import {
  Container,
  Dropdown,
  Image,
  Menu,
} from 'semantic-ui-react';

import {Link} from 'react-router-dom';

const AppBar = () => (
  <Menu fixed='top' inverted>
    <Container>
      <Menu.Item as='a' header>
        <Image size='mini' src='/logo.png' style={{ marginRight: '1.5em' }} />
        Project Name
      </Menu.Item>
      <Menu.Item as='a'>Home</Menu.Item>

      <Dropdown item simple text='Dropdown'>
        <Dropdown.Menu>
          <Dropdown.Item>List Item</Dropdown.Item>
          <Dropdown.Item>List Item</Dropdown.Item>
          <Dropdown.Divider />
          <Dropdown.Header>Header Item</Dropdown.Header>
          <Dropdown.Item>
            <i className='dropdown icon' />
            <span className='text'>Submenu</span>
            <Dropdown.Menu>
              <Dropdown.Item>List Item</Dropdown.Item>
              <Dropdown.Item>List Item</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown.Item>
          <Link to='/evententry'><Dropdown.Item style={{color: "black"}}>Set entries</Dropdown.Item></Link>
        </Dropdown.Menu>
      </Dropdown>
    </Container>
  </Menu>
);

export default AppBar;
