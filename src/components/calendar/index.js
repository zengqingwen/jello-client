import React, {Component} from "react";
import {connect} from 'react-redux';
import {Calendar, Views, momentLocalizer} from 'react-big-calendar';
import moment from 'moment';
import {Button, Header, Image, Modal} from 'semantic-ui-react'

import {fetchEvent} from '../../actions';

import "react-big-calendar/lib/css/react-big-calendar.css";
// require("react-big-calendar/lib/css/react-big-calendar.css");


const localizer = momentLocalizer(moment);
let allViews = Object.keys(Views).map(k => Views[k]);

class MyCalendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentFocusDate: new Date(),
      currentFocusView: 'week',

      events: [],
      title: "",
      start: "",
      end: "",
      desc: "",
      openSlot: false,
      openCrawlEvent: false,
      openSelfEvent: false,
      clickedEvent: {}
    }
  }

  componentDidMount() {
    const {currentFocusDate, currentFocusView} = this.state;
    this.props.fetchEvent(currentFocusDate, currentFocusView);
  }

  changeDate = (e) => {
    this.setState({currentFocusDate: e});
    const {currentFocusView} = this.state;
    if (currentFocusView !== 'agenda') {
      this.props.fetchEvent(e, currentFocusView);
    }
  }

  changeView = (e) => {
    this.setState({currentFocusView: e});
    const {currentFocusDate} = this.state;
    if (e !== 'agenda') {
      this.props.fetchEvent(currentFocusDate, e);
    }
  }

  handleEventSelected(event) {
    console.log("event", event);
    if (event.todoevent === true){
      this.setState({openSelfEvent: true})
    }else {
      this.setState({openCrawlEvent: true})
    }
    this.setState({
      clickedEvent: event,
      start: event.start,
      end: event.end,
      title: event.title,
      desc: event.desc
    });
  }

  close = () => this.setState({openSelfEvent: false, openCrawlEvent: false})

  render() {
    const {currentFocusDate, currentFocusView, openCrawlEvent, openSelfEvent, clickedEvent} = this.state;
    var {events} = this.props;

    // const startDate = moment(end).format('YYYY-MM-DD HH:mm:ss');

    if (events) {
      events.map((item) => {
        item.start = new Date(item.start)
        item.end = new Date(item.end)
        // return null
      })
    }else {
      events = [];
    }


    return (
      <div className="App">
        <Calendar
          localizer={localizer}
          events={events}
          views={allViews}
          showMultiDayTimes
          defaultDate={currentFocusDate}
          defaultView={currentFocusView}
          onNavigate={this.changeDate}
          onView={this.changeView}
          style={{ height: "88vh", paddingTop: "12vh" }}
          onSelectEvent={event => this.handleEventSelected(event)}
        />

        {/*Dimmer Variations*/}
        <Modal dimmer={true} open={openCrawlEvent} onClose={this.close}>
          <Modal.Header>A crawler event.</Modal.Header>
          <Modal.Content image>
            <Modal.Description>
              <Header>{clickedEvent.title}</Header>
              {
                Object.entries(clickedEvent).map(([key, value]) => {
                  if (value instanceof Date) {
                    return <p key={key}>{key}: {moment(value).format('YYYY-MM-DD HH:mm:ss')}</p>
                  }else if(key === 'title') {
                    return null
                  }else {
                    return <p key={key}>{key}: {value}</p>
                  }

                })
              }
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button color='black' onClick={this.close}>
              Close
            </Button>
          </Modal.Actions>
        </Modal>



        <Modal open={openSelfEvent} onClose={this.close}>
          <Modal.Header>A self made event.</Modal.Header>
          <Modal.Content image>
            <Image
              wrapped
              size='medium'
              src='/images/avatar/large/rachel.png'
            />
            <Modal.Description>
              <Header>A self made event.</Header>
              <p>
                We've found the following gravatar image associated with your
                e-mail address.
              </p>
              <p>Is it okay to use this photo?</p>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button color='black' onClick={this.close}>
              Nope
            </Button>
            <Button
              positive
              icon='checkmark'
              labelPosition='right'
              content="Yep, that's me"
              onClick={this.close}
            />
          </Modal.Actions>
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    events: state.event.events,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchEvent: (currentFocusDate, currentFocusView) => {
    dispatch(fetchEvent(currentFocusDate, currentFocusView));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyCalendar);
