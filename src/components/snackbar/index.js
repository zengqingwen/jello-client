import React, {Component} from 'react';
import {connect} from 'react-redux';

import moment from 'moment';

import './styles.css';

class SnackBar extends Component {


  render() {
    const {connectStatus, lastSuccessUpdate} = this.props;
    return (
      <div className={'footer'}>
        <span className={'connect-status'} >Connect status: {connectStatus}</span>
        <span className={'last-update'}>Last succes update: {lastSuccessUpdate ? moment(lastSuccessUpdate).format('YYYY/MM/DD HH:mm:ss') : 'refresh failed'}</span>
      </div>
    )
  }
};

const mapStateToProps = state => {
  return {
    connectStatus: state.event.connectStatus,
    lastSuccessUpdate: state.event.lastSuccessUpdate,
  }
}

export default connect(
  mapStateToProps,
  null,
)(SnackBar);
