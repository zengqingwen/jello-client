import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, Checkbox, Form, Select} from 'semantic-ui-react';

import {Link} from 'react-router-dom';

import {fetchEntry, createOrUpdateEntry, deleteEntry, showAddCredentialModal} from '../../actions';
import CredentialAdd from '../../components/addcredential';

import {bindActionCreators} from 'redux';

class EventEntry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      response: {},
    }
  }

  componentDidMount() {
    this.props.fetchEntry();
    console.log(this.props.entries)
    console.log(this.props.myentries)
    console.log(this.props.myentries2)
    console.log(this.props.mycredentials)
  }

  handleCheck = ( event, {name, checked}) => {
    // console.log(event)
    // console.log(event.target.checked)
    // console.log(checked)
    // console.log(name)

    const { response } = this.state;
    response[`${name}-ismyentry`] = checked;
    this.setState({response});
  }

  handleChange = (e, {name, value}) => {
    const { response } = this.state;
    response[`${name}-token`] = value;
    this.setState({response});
  };

  onSubmit = (event, {name, thisentry}) => {
    const {response} = this.state;
    const {createOrUpdateEntry, deleteEntry} = this.props;
    const entryparm = {
      ...thisentry,
      // ismyentry: response[`${name}-ismyentry`],
      ismyentry: response[`${name}-ismyentry`]!==undefined ? response[`${name}-ismyentry`] : thisentry.ismyentry,
      token_name: response[`${name}-token`] || thisentry.token_name,
    }
    console.log(entryparm)
    console.log(entryparm===thisentry)
    if (entryparm.ismyentry) {
      // post or update
      if (entryparm.token_name === thisentry.token_name) {
        // update
      }else {
        // post
      }
      createOrUpdateEntry(entryparm)
    }else {
      const entryId = entryparm.id
      // delete
      deleteEntry(entryId)
    }
  }

  showAddCredentialModal = () => {
    const {showAddCredentialModal} = this.props;
    showAddCredentialModal();
  }

  render() {
    const {response} = this.state;
    // const {entries, myentries, myentries2, mycredentials} = this.props;
    const {myentries2, mycredentials} = this.props;

    return (
      <div>
        <Button secondary onClick={this.showAddCredentialModal}>Add credential</Button>
        <Link to='/home'>link to home page</Link>
        {

          myentries2.map((entry) => {
            // let ismyentry = entry instanceof Object;
            // let ismyentry2 = entry.token_name !== "";
            // if (ismyentry) {
              return (
                <div key={entry.name}>
                  <Form key={entry.name}>
                    <Form.Group>
                      <Form.Field
                        key={entry}
                        control={Checkbox}
                        label={{ children: entry.name }}
                        name={entry.name}
                        checked={response[`${entry.name}-ismyentry`]!==undefined ? response[`${entry.name}-ismyentry`] : entry.ismyentry}
                        onChange={this.handleCheck}
                      />
                      <Form.Field
                        control={Select}
                        options={mycredentials}
                        name={entry.name}
                        value={response[`${entry.name}-token`] || entry.token_name}
                        onChange={this.handleChange}
                        size="mini"
                      />
                      <Form.Field
                        control={Button}
                        name={entry.name}
                        thisentry={entry}
                        onClick={this.onSubmit}
                      >
                        Submit
                      </Form.Field>
                    </Form.Group>
                  </Form>

                  <CredentialAdd />
                </div>
              )
            // }else {
            //   return <div key={entry}>{entry}22</div>
            // }
          })
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isloading: state.entry.isLoading,
    error: state.entry.error,
    entries: state.entry.entries,
    myentries: state.entry.myentries,
    myentries2: state.entry.myentries2,
    mycredentials: state.entry.mycredentials,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchEntry: () => {
    dispatch(fetchEntry());
  },
  createOrUpdateEntry: (entryparm) => {
    dispatch(createOrUpdateEntry(entryparm));
  },
  deleteEntry: (entryId) => {
    dispatch(deleteEntry(entryId));
  },
  showAddCredentialModal: bindActionCreators(showAddCredentialModal, dispatch)
});


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EventEntry);
