import React, {Component} from "react";
import {Link} from 'react-router-dom';

import AppBar from '../../components/appbar';
import Calendar from '../../components/calendar';
import SnackBar from '../../components/snackbar';

import {getCurrentUserAPI} from '../../api/user';

class Home extends Component {
  componentDidMount() {
    getCurrentUserAPI();
  }

  render() {
    return (
      <div>
        <AppBar />
        {/*<div style={{marginTop: '160px'}} ></div>*/}
        <Calendar />
        This is home page. <Link to='/testpage'>link to test page.</Link>
        <SnackBar />
      </div>
    )
  }
};

export default Home;
