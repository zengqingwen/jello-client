import React, {Component} from "react";
import {connect} from "react-redux";
import {Button} from 'semantic-ui-react';

import {Link} from 'react-router-dom';

import {getCurrentUserAPI} from '../../api/user';

class TestPage extends Component {
  componentDidMount() {
    getCurrentUserAPI();
  }

  render() {
    return (
      <div>
        {this.props.isloading}{this.props.isauthenticated}
        <Button primary>Primary</Button>
        <Button secondary>Secondary</Button>
        <Link to='/home'>link to home page</Link>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isloading: state.auth.isLoading,
    isauthenticated: state.auth.isAuthenticated
  };
};

export default connect(
  mapStateToProps,
  null
)(TestPage);
