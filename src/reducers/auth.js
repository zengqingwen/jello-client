import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from '../actions/types';

const loginInitialState = {
  isLoading: false,
  isAuthenticated: false,
  username: null,
  name: null,
  avatar: null,
  token: null,
  isStaff: false,
  error: null,
};

const initialState = {
  ...loginInitialState,
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
        return {
          ...state,
          isLoading: true,
          isAuthenticated: false,
          error: null,
        };
    case LOGIN_SUCCESS:
        return {
          ...state,
          isLoading: false,
          isAuthenticated: true,
          token: action.token,
          username: action.username,
          avatar: action.avatar,
          name: action.name,
          isStaff: action.isStaff,
          error: null,
        };
    case LOGIN_FAILURE:
        return {
          ...state,
          ...loginInitialState,
          error: action.error,
        };
    default:
      return state;
  }
};

export default auth;
