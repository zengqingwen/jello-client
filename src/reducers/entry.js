import {
  FETCH_ENTRY_REQUEST,
  FETCH_ENTRY_SUCCESS,
  FETCH_ENTRY_FAILURE,

  CREATE_CREDENTIAL_REQUEST,
  CREATE_CREDENTIAL_SUCCESS,
  CREATE_CREDENTIAL_FAILURE,
  FETCH_CREDENTIAL_SUCCESS,
} from '../actions/types';

const initState = {
  isLoading: false,
  entries: null,
  myentries: null,
  myentries2: [],
  mycredentials: null,
  error: null,
}

const entry = (state = initState, action) => {
  switch (action.type) {
    case FETCH_ENTRY_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case FETCH_ENTRY_SUCCESS:
      return {
        ...state,
        entries: action.entry.entries,
        myentries: action.entry.myentries,
        myentries2: action.entry.myentries2,
        mycredentials: action.entry.mycredentials,
      };
    case FETCH_ENTRY_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };

    case FETCH_CREDENTIAL_SUCCESS:
      return {
        ...state,
        mycredentials: action.credential,
      };
    default:
      return state;
  }
}

export default entry;
