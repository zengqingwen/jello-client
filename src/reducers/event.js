import {
  FETCH_EVENT_REQUEST,
  FETCH_EVENT_SUCCESS,
  FETCH_EVENT_FAILURE,
} from '../actions/types';

const initState = {
  isLoading: false,
  shortcuts: null,
  events: null,
  errors: null,
  error: null,
  connectStatus: 'disconnect',
  lastSuccessUpdate: null,
}

const event = (state = initState, action) => {
  switch (action.type) {
    case FETCH_EVENT_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
        connectStatus: 'connecting...',
      };
    case FETCH_EVENT_SUCCESS:
      return {
        ...state,
        shortcuts: action.event.shortcut_outputs,
        events: action.event.events_outputs,
        errors: action.event.err_and_excepts,
        connectStatus: 'success',
        lastSuccessUpdate: new Date(),
      };
    case FETCH_EVENT_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
        connectStatus: 'disconnect',
        lastSuccessUpdate: null,
      };
    default:
      return state;
  }
}

export default event;
