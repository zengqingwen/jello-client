import {combineReducers} from "redux";

import auth from './auth';
import register from './register';
import event from './event';
import entry from './entry';
import switchiy from './switchiy';


const rootReducer = combineReducers({
  auth,
  register,
  event,
  entry,
  switchiy,
});

export default rootReducer;
