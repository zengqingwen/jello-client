import {
  SHOW_ADD_CREDENTIAL_MODAL,
} from '../actions/types';

const initialState = {
  addCredentialModal: false,
}

const switchiy = (state=initialState, action) => {
  switch (action.type) {
    case(SHOW_ADD_CREDENTIAL_MODAL):
      return {
        // ...state,
        addCredentialModal: !state.addCredentialModal,
      };
    default:
      return state;
  }
}

export default switchiy ;